
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import com.lern.calculatormvc.R;

public class Message {

    public static AlertDialog.Builder alert(Activity activity, final int id) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        alertDialog.setTitle(id);
        alertDialog.setCancelable(false);
        return alertDialog;
    }
}
