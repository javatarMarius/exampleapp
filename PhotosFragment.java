import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PhotosFragment extends ListFragment {

    private PhotosAdapter adapter;
    private Observable<List<Photo>> lastRequest;
    private Subscription subscription;

    public static LikedPhotosFragment newInstance() {
        return new LikedPhotosFragment();
    }

    public LikedPhotosFragment() {
        setRetainInstance(true);
    }    

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setDividerHeight(0);

        if (adapter == null) {
            adapter = new PhotosAdapter(this);
        }
        setListAdapter(adapter);

        if (lastRequest != null) {
            subscribe();
        } else if (adapter.isEmpty()) {
            makeRequest();
            subscribe();
        }
    }

    private void makeRequest() {
        lastRequest = Storage.get().photos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private void onRequestCompletion() {
        lastRequest = null;
        setListShown(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (subscription != null) {
            subscription.unsubscribe();
            subscription = null;
        }
    }
}
