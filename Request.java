import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Map;

public class Request {

    private static AsyncHttpClient client;
    private static IRequestDone iRequestDone;
    private static HashMap<String, String> headerMap;
    private static HashMap<String, IRequestDone> callbacksList;

    static {
        client = new AsyncHttpClient();
        client.setResponseTimeout(35000);
        client.setConnectTimeout(35000);
        client.setMaxRetriesAndTimeout(0, 0);
        headerMap = new HashMap<>();
        callbacksList = new HashMap<>();
        addHeaders();
        initSsl();
    }
	
	private void initSsl(){
		try {
            SSLSocketFactory sslSocketFactory = getSSLSocketFactory();
            client.setSSLSocketFactory(sslSocketFactory);
        } catch (Exception e) {
            e.printStackTrace();
        }	
	}

    private static SSLSocketFactory getSSLSocketFactory() throws Exception {
        StringBuilder sb = new StringBuilder(Constants.X).append(Constants.Y).append(Constants.Z);
        KeyStore keyStore = KeyStore.getInstance("BKS");
        InputStream instream = Application.getInstream();
        keyStore.load(instream, sb.toString().toCharArray());
        AppSslSocketFactory sslSocketFactory = new AppSslSocketFactory(keyStore);
        sslSocketFactory.setHostnameVerifier(MySSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
        return sslSocketFactory;
    }

    private static void addHeaders() {
        client.addHeader("Accept", "*/*");
        client.addHeader("Cache-Control", "no-cache");
        client.addHeader("Cookie", "XDEBUG_SESSION=debug");
    }

    public static AsyncHttpClient getClient() {
        return client;
    }

    public static void removeCustomHeaders(final HashMap<String, String> headers) {
        if (headers != null && !headers.isEmpty()) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                client.removeHeader(entry.getKey());
            }
        }
    }

    public static void post(final String requestUrl,
                            final HashMap<String, String> headers,
                            final HashMap<String, String> params,
                            final IRequestDone requestDone) {

        iRequestDone = requestDone;
        saveCallbacks(requestUrl);
        LoopJService.post(requestUrl, headers, getParams(params), new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                if (statusCode == 200 || statusCode == 201) {
                    if (callbacksList.containsKey(requestUrl)) {
                        callbacksList.get(requestUrl).onSuccessResponse(new SuccessData(statusCode, requestUrl, response, null, null));
                    }
                }
                iRequestDone = null;
            }    

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);                
                if (callbacksList.containsKey(requestUrl)) {
                    callbacksList.get(requestUrl).onErrorResponse(new ErrorData(statusCode, throwable, null, errorResponse, null, requestUrl));
                }
            }
        });
    }

    public static void delete(final Context context, final String requestUrl,
                              final HashMap<String, String> headers,
                              final HashMap<String, String> params,
                              final IRequestDone requestDone) {

        iRequestDone = requestDone;
        saveCallbacks(requestUrl);

        LoopJService.delete(context, requestUrl, headers, getParams(params), new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                responseString.toString();
                if (callbacksList.containsKey(requestUrl)) {
                    callbacksList.get(requestUrl).onErrorResponse(
                            new ErrorData(statusCode, throwable, null, null, responseString, requestUrl));
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                responseString.toString();
                if (statusCode == 200 || statusCode == 201) {
                    if (callbacksList.containsKey(requestUrl)) {
                        SuccessData successData = null;
                        try {
                            successData = new SuccessData(statusCode, requestUrl, new JSONObject(responseString), null, null);
                            callbacksList.get(requestUrl).onSuccessResponse(successData);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    public static void postWithFile(
            final String url, final HashMap<String, String> headers,
            RequestParams params, final IRequestDone requestDone) {

        iRequestDone = requestDone;
        saveCallbacks(url);
        LoopJService.post(url, headers, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.e("LOG_TAG", "OK 1");
                if (statusCode == 200 || statusCode == 201) {
                    if (callbacksList.containsKey(url)) {
                        callbacksList.get(url).onSuccessResponse(
                                new SuccessData(statusCode, url, response, null, null));
                    }
                }
            }   

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                if (callbacksList.containsKey(url)) {
                    callbacksList.get(url).onErrorResponse(
                            new ErrorData(statusCode, throwable, null, errorResponse, null, url));
                }
            }
        });
    }

    public static void get(final String requestUrl, final IRequestDone requestDone, final HashMap<String, String> headers, HashMap params) {

        iRequestDone = requestDone;
        saveCallbacks(requestUrl);
        LoopJService.get(requestUrl, getParams(params), headers, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.e("LOG_TAG", "URL " + "" + requestUrl + " = " + response.toString());
                if (callbacksList.containsKey(requestUrl)) {
                    callbacksList.get(requestUrl).onSuccessResponse(new SuccessData(statusCode, requestUrl, response, null, null));
                }
            } 

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.e("LOG_TAG", "onFailure URL " + "" + requestUrl + " statusCode  = " + statusCode);
                if (iRequestDone != null) {
                    if (callbacksList.containsKey(requestUrl)) {
                        callbacksList.get(requestUrl).onErrorResponse(new ErrorData(statusCode, throwable, null, errorResponse, null, requestUrl));
                    }
                }
            }          
        });
    }

    private static void saveCallbacks(final String requestUrl) {
        if (!callbacksList.containsKey(requestUrl)) {
            callbacksList.put(requestUrl, iRequestDone);
        } else {
            callbacksList.remove(requestUrl);
            callbacksList.put(requestUrl, iRequestDone);
        }
    }

    private static RequestParams getParams(final HashMap<String, String> params) {
        if (params != null && !params.isEmpty()) {
            return new RequestParams(params);
        }
        return null;
    }

    private static void clearStringParams(final HashMap<String, String> params) {
        if (params != null && !params.isEmpty()) {
            params.clear();
        }
    }

    private static void clearMultipartDataParams(final HashMap<String, File> params) {
        if (params != null && !params.isEmpty()) {
            params.clear();
        }
    }

    private static final class LoopJService {

        @Deprecated
        public static void get(final String url, final RequestParams params, AsyncHttpResponseHandler responseHandler) {
            client.get(url, params, responseHandler);
        }

        public static void get(final String url, final RequestParams params, final HashMap<String, String> headers,
                               AsyncHttpResponseHandler responseHandler) {

            setHeaders(headers);
            client.get(url, params, responseHandler);
        }

        public static void post(final String url, final HashMap<String, String> headers,
                                final RequestParams params, AsyncHttpResponseHandler responseHandler) {

            setHeaders(headers);
            client.post(url, params, responseHandler);
        }

        public static void delete(Context context, final String url, final HashMap<String, String> headers,
                                  final RequestParams params, ResponseHandlerInterface responseHandlerInterface) {

            setHeaders(headers);
            client.delete(context, url, null, params, responseHandlerInterface);
        }

        public static void removeCustomHeaders(final HashMap<String, String> headers) {
            if (headers != null && !headers.isEmpty()) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    client.removeHeader(entry.getKey());
                }
            }
        }

        private static void setHeaders(final HashMap<String, String> headers) {
            if (headers != null && !headers.isEmpty()) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    client.addHeader(entry.getKey(), entry.getValue());
                }
            }
        }
    }
}
