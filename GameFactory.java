package com.example.TestApp.factory.game;

import com.example.TestApp.MyActivity;
import com.example.TestApp.factory.total.SceneProperties;
import com.example.TestApp.managers.ActivityManager;
import com.example.TestApp.scene.SceneName;
import com.example.TestApp.util.DestroyManager;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.io.IOException;

public class GameFactory {

    public static Sprite create(float x, float y, String path, SceneName sceneName) throws IOException {
        Sprite tempSprite = null;
        final MyActivity activity = ActivityManager.getInstance().getActivity();
        final VertexBufferObjectManager vbom = activity.getVertexBufferObjectManager();
        final ITextureRegion region = GameTextureRegionFactory.create(path, activity);
        tempSprite = new Sprite(x, y, region, vbom);
        SceneProperties.sceneProp(sceneName, tempSprite);
        DestroyManager.objectList.add(tempSprite);
        return tempSprite;
    }
}
