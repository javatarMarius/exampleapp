

import android.util.Log;
import java.util.ArrayList;

public class AppObservable implements ISubject {

    private ArrayList<IObserver> mObservers;
    private static AppObservable sInstance;

    public static AppObservable getInstance() {
        if (sInstance == null) {
            sInstance = new AppObservable();
        }
        return sInstance;
    }

    private AppObservable() {

        mObservers = new ArrayList<>();
    }


    @Override
    public void addObserver(IObserver iObserver) {
        if (!mObservers.contains(iObserver)) {
            mObservers.add(iObserver);
        }
    }

    @Override
    public void removeObserver(IObserver iObserver) {
        final int i = mObservers.indexOf(iObserver);
        if (i >= 0) {
            mObservers.remove(iObserver);
        }
    }

    @Override
    public void removeAllObservers() {
        if (mObservers != null) {
            mObservers.clear();
        }
    }

    @Override
    public void notifyStarted() {
        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).onStartRequest();
        }
    }

    @Override
    public void notifyStartedWithAction(final int action) {
        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).onStartRequest(action);
        }
    }

    @Override
    public void notifySuccess(int action, Object o) {
        for (int i = 0; i <  mObservers.size(); i++) {
            mObservers.get(i).onSuccess(action, o);
        }
    }

    @Override
    public void notifyFailed(IErrorData iErrorData) {
        final int size = mObservers.size();
        for (int i = 0; i < size; i++) {
            mObservers.get(i).onFail(iErrorData);
        }
    }

    @Override
    public boolean containObserver(IObserver iObserver) {
        return mObservers.contains(iObserver);
    }
}
