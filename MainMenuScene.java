package com.example.TestApp.scene;

import android.view.KeyEvent;
import com.example.TestApp.Const;
import com.example.TestApp.R;
import com.example.TestApp.command.BaseButton;
import com.example.TestApp.command.ISkelet;
import com.example.TestApp.factory.total.AlertFactory;
import com.example.TestApp.factory.total.ButtonFactory;
import com.example.TestApp.managers.ButtonManager;
import com.example.TestApp.pref.GamePref;
import com.example.TestApp.pref.PrefConst;
import com.example.TestApp.util.CreateObjects;
import com.example.TestApp.util.DestroyManager;
import com.example.TestApp.util.Flag;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class MainMenuScene extends BaseScene implements ISkelet {

    private BaseButton musicButton;
    public static Sprite wolfFlag;
    public static Sprite rabbitFlag;

    public MainMenuScene() {
    }

    @Override
    public boolean backPressedButton(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                final String title = getResGame(R.string.exitGame);
                final String negativeButton = getResGame(R.string.no);
                final String positiveButton = getResGame(R.string.yes);
                AlertFactory.exitAlert(title, positiveButton, negativeButton).show();
                break;
        }
        return false;
    }

    @Override
    public boolean onSceneTouchEvent(Scene scene, TouchEvent touchEvent) {
        final int touch = touchEvent.getAction();
        switch (touch) {
            case TouchEvent.ACTION_DOWN:
                break;
            case TouchEvent.ACTION_MOVE:
                break;
            case TouchEvent.ACTION_UP:
                break;
        }
        return false;
    }

    @Override
    public boolean onAreaTouched(TouchEvent touchEvent, ITouchArea iTouchArea, float v, float v2) {
        BaseButton button = (BaseButton) iTouchArea;
        final int id = button.getIdButton();
        if (touchEvent.getMotionEvent().getPointerCount() == 1) {
            if (touchEvent.isActionUp()) {
                ButtonManager.getInstance().buttonWasPressed(id);
            }
        }
        return false;
    }

    @Override
    public void initItems() throws IOException, XmlPullParserException {
        createBackground();
        getButtonParser().parseXmlFile(R.xml.main_menu_buttons);

        final int lengh = Const.ARR_MAIN_MENU_KEY.length;
        for (int i = 0; i < lengh; i++) {
            ButtonFactory.create(Const.ARR_MAIN_MENU_KEY[i]);
        }

        musicButton = ButtonFactory.create(Const.MUSIC);
        musicButtonSettings();
        //если есть выбранный флаг(и), то загрузить их на сцену
        CreateObjects.createMenuObject();
        Flag.createMenuFlags();
    }

    private void musicButtonSettings() {
        if (GamePref.getInstance().getBoolPref(PrefConst.MUSIC)) {
            musicButton.setAlpha(Const.SOUND_MUSIC_ON);
        } else {
            musicButton.setAlpha(Const.SOUND_MUSIC_OFF);
        }
    }

    public void destroyWolfFlag() {
        if (wolfFlag != null) {
            DestroyManager.destroy(wolfFlag, MainMenuScene.this);
            wolfFlag = null;
        }
    }

    public void destroyRabbitFlag() {
        if (rabbitFlag != null) {
            DestroyManager.destroy(rabbitFlag, MainMenuScene.this);
            rabbitFlag = null;
        }
    }

    private void createBackground() throws IOException {
        createKapak(SceneName.MAIN_MENU_SCENE, R.string.main_menu_bg_1, R.string.main_menu_bg_3,
                R.string.main_menu_bg_2, R.string.main_menu_bg_4);
    }
}
