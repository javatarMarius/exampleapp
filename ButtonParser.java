package com.example.TestApp.parser;

import com.example.TestApp.managers.ActivityManager;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class ButtonParser extends Parser {

    public ButtonParser() {
    }

    public int getId(String key) {
        return DataParser.getInstance().getIdHashMap().get(key);
    }

    public float getX(String key) {
        return DataParser.getInstance().getXHashMap().get(key);
    }

    public float getY(String key) {
        return DataParser.getInstance().getYHashMap().get(key);
    }

    public String getScene(String key) {
        return DataParser.getInstance().getSceneHashMap().get(key);
    }

    public String getPath(String key) {
        return DataParser.getInstance().getPathHashMap().get(key);
    }

    public void parseXmlFile(int path) throws XmlPullParserException, IOException {
        String textValue = "";
        String buttonKey = "";
        XmlPullParser parser = prepareXpp(path);
        while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
            switch (parser.getEventType()) {

                case XmlPullParser.START_TAG:
                    for (int i = 0; i < parser.getAttributeCount(); i++) {
                        if (parser.getAttributeName(i).equalsIgnoreCase("key_name")) {
                            buttonKey = parser.getAttributeValue(i);
                        }
                    }
                    break;
                case XmlPullParser.TEXT:
                    textValue = parser.getText();
                    break;
                case XmlPullParser.END_TAG:
                    readData(textValue, buttonKey, parser);
                    break;
            }
            parser.next();
        }
    }

    private XmlPullParser prepareXpp(int path) {
        return ActivityManager.getInstance().getActivity().getResources().getXml(path);
    }

    private void readData(final String textValue, final String buttonKey, XmlPullParser parser) {

        if (parser.getName().equalsIgnoreCase("id")) {
            int tmpId = Integer.parseInt(textValue);
            DataParser.getInstance().getIdHashMap().put(buttonKey, tmpId);
        }
        if (parser.getName().equalsIgnoreCase("x")) {
            float tmpX = Float.parseFloat(textValue);
            DataParser.getInstance().getXHashMap().put(buttonKey, tmpX);
        }
        if (parser.getName().equalsIgnoreCase("y")) {
            float tmpY = Float.parseFloat(textValue);
            DataParser.getInstance().getYHashMap().put(buttonKey, tmpY);
        }
        if (parser.getName().equalsIgnoreCase("scene")) {
            DataParser.getInstance().getSceneHashMap().put(buttonKey, textValue);
        }
        if (parser.getName().equalsIgnoreCase("path")) {
            DataParser.getInstance().getPathHashMap().put(buttonKey, textValue);
        }
    }
}
