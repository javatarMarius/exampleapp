
public class ModelCalculator {

    private int mSumResult;
    private int mDivisionResult;
    private int mSubtractionResult;
    private int mMultiplicationResult;

    public int getSumResult() {
        return mSumResult;
    }

    public int getSubtractionResult() {
        return mSubtractionResult;
    }

    public int getMultiplicationResult() {
        return mMultiplicationResult;
    }

    public int getDivisionResult() {
        return mDivisionResult;
    }

    public void addition(int firstNamber, int secondNumber) {
        mSumResult = firstNamber + secondNumber;
    }

    public void subtraction(int firstNamber, int secondNumber) {
        mSubtractionResult = firstNamber - secondNumber;
    }

    public void multiplication(int firstNamber, int secondNumber) {
        mMultiplicationResult = firstNamber * secondNumber;
    }

    public boolean division(int firstNamber, int secondNumber) {
        if (secondNumber == 0) {
            return false;
        } else {
            mDivisionResult = firstNamber / secondNumber;
            return true;
        }
    }
}
