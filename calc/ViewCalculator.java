
import android.app.Activity;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.lern.calculatormvc.R;
import com.lern.calculatormvc.utils.Message;

public class ViewCalculator {

    private TextView mTvAnsver;
    private Activity mActivity;
    private EditText mFirstNumber;
    private EditText mSecondNumber;
    private Button mBtnEqual;
    private Button mBtnAddition;
    private Button mBtnDivision;
    private Button mBtnSubtraction;
    private Button mBtnMultiplication;
    private TextView mOperatorSymbolId;
//    private Message message;

    private Button[] arrayButtons;

    public ViewCalculator(Activity activity) {
        this.mActivity = activity;
        initViews();
    }

    public int getFirstNumber() {
        isEmptyField(mFirstNumber);
        return Integer.parseInt(String.valueOf(mFirstNumber.getText()));
    }

    public int getSecondNumber() {
        isEmptyField(mSecondNumber);
        return Integer.parseInt(String.valueOf(mSecondNumber.getText()));
    }

    public int getAnsver() {
        return Integer.parseInt(String.valueOf(mTvAnsver.getText()));
    }

    public void setAnsver(final int ansver) {
        this.mTvAnsver.setText(String.valueOf(ansver));
    }

    public String getOperatorSymbolId() {
        return String.valueOf(mOperatorSymbolId.getText());
    }

    public void setOperatorSymbolId(final String str) {
        this.mOperatorSymbolId.setText(str);
    }

    public void execute(View.OnClickListener onClickListener) {
        int listSize = arrayButtons.length;
        for (int i = 0; i < listSize; i++) {
            arrayButtons[i].setOnClickListener(onClickListener);
        }
    }

    public void showDivisionError() {
        Message.alert(mActivity, R.string.divide_by_zero).
                setPositiveButton("fix", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mSecondNumber.setText("");
                    }
                }).show();
    }

    public boolean isEmptyField(EditText number) {
        if (number.getText().length() == 0) {
            number.setText("0");
            return true;
        } return false;  
    }

    private void initViews() {
        mTvAnsver = (TextView) mActivity.findViewById(R.id.tvAnsver);
        mFirstNumber = (EditText) mActivity.findViewById(R.id.firstNumberId);
        mSecondNumber = (EditText) mActivity.findViewById(R.id.secondNumberId);
        mOperatorSymbolId = (TextView) mActivity.findViewById(R.id.operationSymbolId);
        addInArray();
        final int length = IdArrays.ID.length;
        for (int i = 0; i < length; i++) {
            arrayButtons[i] = (Button) mActivity.findViewById(IdArrays.ID[i]);
        }
    }

    private void addInArray() {
        arrayButtons = new Button[]{mBtnEqual, mBtnDivision, mBtnAddition, mBtnSubtraction,
                mBtnMultiplication
        };
    }
}
