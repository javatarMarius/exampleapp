import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FetchPublication implements IRequestDone {

    private ExecutorService executorService;

    public FetchPublication(IObserver iObserver) {

        AppObservable.getInstance().addObserver(iObserver);
    }

    @Override
    public void onErrorResponse(ErrorData errorData) {
        AppObservable.getInstance().notifyFailed(errorData);
    }

    @Override
    public void onSuccessResponse(SuccessData successData) {
        parseResponse(successData);
    }

    public void fetchPublication(final String url, final HashMap<String, String> headers) {
        AppObservable.getInstance().notifyStartedWithAction(Action.GET_PUBLICATION);
        Request.get(url, this, headers, null);
    }

    private void parseResponse(final SuccessData successData) {
        executorService = Executors.newCachedThreadPool();
        final Future publications = execute(successData);
        notifyObservers(publications);
    }

    private Future execute(final SuccessData successData) {
        return executorService.submit(new Callable() {
            public Object call() throws Exception {
                String message = successData.getSuccessObject().getString("message");
                final JSONArray jsonArray = successData.getSuccessObject().getJSONArray("data");
                final Object o = parsePublication(jsonArray);
                return new Pair(message, o);
            }
        });
    }

    private void notifyObservers(final Future publications) {
        try {
            AppObservable.getInstance().notifySuccess(Action.GET_PUBLICATION, publications.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            executorService.shutdownNow();
        }
    }

    private List<Publication> parsePublication(final JSONArray jsonArray) {
        final Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Type itemsListType = new TypeToken<List<Publication>>() {
        }.getType();
        return gson.<ArrayList<Publication>>fromJson(jsonArray.toString(), itemsListType);
    }
}
