import android.app.Application;

import com.bumptech.glide.Glide;
import com.bumptech.glide.integration.okhttp.OkHttpUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.squareup.okhttp.OkHttpClient;

import java.io.InputStream;
import java.util.concurrent.TimeUnit;
import timber.log.Timber;

public class Application extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        initOkhttpClient();
        initGlide();
    }
	
	private void initOkhttpClient(){
		final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);
	}
	
	private void initGlide(){
		Glide.get(this).register(GlideUrl.class,InputStream.class, 
				new OkHttpUrlLoader.Factory(okHttpClient));
	}
}
