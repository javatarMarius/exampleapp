
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class PhotosActivity extends Activity {

    public static void start(final Context context) {
        final Intent i = new Intent(context, PhotosActivity.class);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liked_photos);

        if (savedInstanceState == null) {
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.content, PhotosFragment.newInstance())
                    .commit();
        }
    }
}
