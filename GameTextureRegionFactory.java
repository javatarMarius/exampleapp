package com.example.TestApp.factory.game;

import android.content.Context;
import com.example.TestApp.factory.menu.AtlasFactory;
import com.example.TestApp.util.DestroyManager;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.ui.activity.BaseGameActivity;

import java.io.IOException;

public final class GameTextureRegionFactory {

    public static ITextureRegion create(String path, BaseGameActivity activity) throws IOException {
        ITextureRegion tempTextureRegion = null;
        final Context context = activity.getBaseContext();
        BuildableBitmapTextureAtlas tempAtlas = GameAtlasFactory.create(path, activity);
        tempTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(tempAtlas, context, path);
        GameAtlasFactory.atlasSettings(tempAtlas);
        DestroyManager.regionList.add(tempTextureRegion);
        return tempTextureRegion;
    }
}


















